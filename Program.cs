﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asm1
{
    class Program
    {
        static ArrayList giaoVien = new ArrayList();
        static void Main(string[] args)
        {
            ArrayList giaoVien = new ArrayList();
            int choice = 0;
            do
            {
                Console.WriteLine("1.Nhap thong tin nhan vien.");
                Console.WriteLine("2.In danh sach nhan vien.");
                Console.WriteLine("3.Nhan vien co luong thap nhat.");
                Console.WriteLine("4.Thoat.");
                Console.Write("Lua chon: ");

                if (int.TryParse(Console.ReadLine(), out choice))
                {
                    switch (choice)
                    {
                        case 1:
                            NhapThongTinNhanVien();
                            break;
                        case 2:
                            InDanhSachNhanVien();
                            break;
                        case 3:
                            TimNhanVienLuongThapNhat();
                            break;
                        case 4:
                            Console.WriteLine("Ket thuc chuong trinh.");
                            break;
                        default:
                            Console.WriteLine("Lua chon khong hop le. Vui long thu lai.");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Lua chon khong hop le. Vui long thu lai.");
                }
            } while (choice != 4);

        }

        private static void TimNhanVienLuongThapNhat()
        {
            if (giaoVien.Count == 0)
            {
                Console.WriteLine("Danh sach nhan vien rong.");
                return;
            }
            double luongCoBanThapNhat = ((GiaoVien)giaoVien[0]).LuongCoBan;
            GiaoVien gv = (GiaoVien)giaoVien[0];
            foreach (var giaoVienItem in giaoVien)
            {
                GiaoVien gv1 = (GiaoVien)giaoVienItem;

                if (gv1.LuongCoBan < luongCoBanThapNhat) { 
                    luongCoBanThapNhat = gv1.LuongCoBan;
                    gv = gv1;
                }
            }
            Console.WriteLine($"Ho ten: {gv.HoTen}, Nam sinh: {gv.NamSinh},Luong co ban: {gv.LuongCoBan},He so luong:{gv.HeSoLuong},Tong Luong:{gv.TinhLuong()}");
            Console.WriteLine("Successfull!");
        }

        private static void InDanhSachNhanVien()
        {
            if (giaoVien.Count == 0)
            {
                Console.WriteLine("Danh sach nhan vien rong.");
                return;
            }
            int number = 0;
            foreach (var giaoVienItem in giaoVien)
            {
                number++;
                GiaoVien gv = (GiaoVien)giaoVienItem;
                Console.WriteLine($"{number}.Ho ten: {gv.HoTen}, Nam sinh: {gv.NamSinh},Luong co ban: {gv.LuongCoBan},He so luong:{gv.HeSoLuong},Tong Luong:{gv.TinhLuong()}");

            }
            Console.WriteLine("Successfull!");
        }

        private static void NhapThongTinNhanVien()
        {
            Console.Write("Nhap so luong giao vien:");
            int numberTeacher;
            while (!int.TryParse(Console.ReadLine(), out numberTeacher) || numberTeacher <= 0)
            {
                Console.Write("Fail!");
                Console.Write("Nhap lai:");
            }
            for (int i = 0; i < numberTeacher; i++)
            {
                GiaoVien giaoVienInput = new GiaoVien();
                giaoVienInput.NhapThongTin();
                double heSoLuong;
                Console.Write("Nhap he so luong:");
                while (!double.TryParse(Console.ReadLine(), out heSoLuong) || heSoLuong <= 0)
                {
                    Console.WriteLine("Fail!");
                    Console.Write("Nhap lai:");
                }
                giaoVienInput.NhapThongTin(heSoLuong);
                giaoVien.Add(giaoVienInput);
                Console.WriteLine("Them nhan vien thanh cong.");
            }
        }
    }
}
