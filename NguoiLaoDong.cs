﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asm1
{
     class NguoiLaoDong
    {

        public string HoTen { get; set; }
        public int NamSinh { get; set; }
        public double LuongCoBan { get; set; }

        public NguoiLaoDong()
        {
        }

        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }

        public void NhapThongTin()
        {
            Console.Write("Nhap ho ten: ");
            string hoTen;
            hoTen = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(hoTen) || hoTen.Any(char.IsDigit))
            {
                Console.WriteLine("Fail!! khong duoc bo trong va chua chu so trong ho va ten ");
                Console.Write("Nhap lai ho ten:");
                hoTen = Console.ReadLine();

            }
            HoTen = hoTen.Trim();
           

            Console.Write("Nhap nam sinh: ");
            int namSinh;
            while (!int.TryParse(Console.ReadLine(), out namSinh) || namSinh <= 1900 || namSinh > DateTime.Now.Year)
            {
                Console.WriteLine("Fail!! Nam sinh phai lon 1900 va be hon thoi gian hien tai");
                Console.Write("Nhap lai nam sinh:");
            }
            NamSinh = namSinh;

            Console.Write("Nhap luong co ban:");
            double luongCoBan;
            while (!double.TryParse(Console.ReadLine(), out luongCoBan) || luongCoBan <= 0)
            {
                Console.WriteLine("Fail!!");
                Console.Write("Nhap lai luong co ban:");
            }
            LuongCoBan = luongCoBan;
        }

        public double TinhLuong()
        {
            return LuongCoBan;
        }

        public virtual void XuatThongTin()
        {
            Console.WriteLine($"Ho ten la: : {HoTen}, nam sinh: {NamSinh}, luong co ban: {LuongCoBan}");
        }

    }
}

