﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asm1
{
    class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong { get; set; }

        public GiaoVien()
        {

        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
            HeSoLuong = heSoLuong;
        }

        public void NhapThongTin(double heSoLuong) 
        {
            HeSoLuong = heSoLuong;
        }
        
        public double TinhLuong() { 
            return LuongCoBan*HeSoLuong*1.25;
        }

        public void XuatThongTin() {
            Console.WriteLine($"Hệ số lương: {HeSoLuong}, Lương: {TinhLuong()}");
        }
        public void XuLy()
        {
            HeSoLuong += 0.6;
        }
    }
}
